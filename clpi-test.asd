;;;; clpi-test System Definition

#-:asdf3.2
(error "Requires ASDF >=3.2")

;; Not necessary, but easier to have when using SLIME.
(in-package :asdf-user)

(defsystem #:clpi-test
  :description "Test system for clpi system."
  :license "BSD-2-Clause"
  :pathname "test/"
  :class :package-inferred-system
  :depends-on (#:clpi-test/clpi-test))
