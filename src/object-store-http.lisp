;;;; HTTP based object store
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/object-store-http
    (:use #:cl
          #:alexandria
          #:clpi/defs
          #:clpi/object-store
          #:clpm-multi-http-client)
  (:import-from #:puri)
  (:export #:http-object-store))

(in-package #:clpi/object-store-http)

(defclass http-object-store (object-store)
  ((url
    :initarg :url
    :accessor http-object-store-url
    :documentation
    "A PURI URL pointing to the root directory of the index.")
   (http-client
    :initarg :http-client
    :initform nil)))

(defmethod initialize-instance :after ((object-store http-object-store) &rest initargs)
  (declare (ignore initargs))
  (if (puri:uri-p (http-object-store-url object-store))
      (setf (http-object-store-url object-store)
            (puri:copy-uri (http-object-store-url object-store)))
      (setf (http-object-store-url object-store)
            (puri:parse-uri (http-object-store-url object-store))))
  (setf (http-object-store-url object-store)
        (puri:merge-uris "clpi/v0.4/" (http-object-store-url object-store))))

(defun http-object-store-http-client (object-store)
  (or (let ((client (slot-value object-store 'http-client)))
        (if (functionp client)
            (funcall client)
            client))
      *default-http-client*))

(defmethod open-object-stream ((object-store http-object-store) path (direction (eql :input))
                               &key binary-p offset
                                 (if-does-not-exist :error) if-exists)
  (declare (ignore if-exists))
  (when (and offset (not binary-p))
    ;; Technically we can offset if it's not binary, but its not very efficient
    ;; (we'd have to transfer the entire prefix from the server to robustly
    ;; handle variable width encodings).
    (error "Cannot offset object stream unless it's binary."))
  (let ((uri (puri:merge-uris path (http-object-store-url object-store)))
        headers)
    (when (and offset (plusp offset))
      ;; Subtract one from the offset to avoid issues with the start of the
      ;; range being beyond the end of the resource. This lets us avoind having
      ;; to do a HEAD to figure out the size.
      (push (cons :range (format nil "bytes=~D-" (1- offset))) headers)
      ;; Explicitly ask for no compression.
      (push (cons :accept-encoding "identity") headers))
    (multiple-value-bind (s code)
        (http-client-request (http-object-store-http-client object-store) uri
                             :additional-headers headers
                             :want-stream t
                             :force-binary binary-p)
      (cond
        ((= code 404)
         (ecase if-does-not-exist
          ((nil)
           (signal 'object-missing :store object-store :path path)
           (close s)
           (return-from open-object-stream nil))
          (:error
           (close s)
           (cerror "Continue" 'object-missing :store object-store :path path))))
        ((>= code 400)
         ;; Don't know what happened.
         (close s)
         (error "Don't know how to handle response ~D for ~A" code uri))
        ((and (= code 200) offset (plusp offset))
         ;; The server didn't understand our request... Need to forward the
         ;; stream ourselves. Should probably emit a warning here to let the
         ;; user know they're using an awful server.
         (dotimes (i offset)
           (read-byte s)))
        ((= code 206)
         ;; Got partial content back! Pop off the one extra byte we requested.
         (read-byte s)))
      s)))
