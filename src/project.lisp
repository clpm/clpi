;;;; Base project implementation
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/project
    (:use #:cl
          #:alexandria
          #:anaphora
          #:clpi/defs
          #:clpi/release
          #:clpi/repos
          #:clpi/utils)
  (:export #:project)
  (:import-from #:cl-semver))

(in-package #:clpi/project)

(defclass project ()
  ((index
    :initarg :index
    :reader index)
   (name
    :initarg :name
    :reader project-name)

   (release-ht
    :accessor project-release-ht)
   (repo
    :accessor project-repo)

   (releases-object-version
    :initarg :releases-object-version
    :accessor project-releases-object-version)
   (version-scheme
    :initarg :version-scheme
    :accessor project-version-scheme)

   (release-data-read-p
    :initform nil
    :accessor project-release-data-read-p)
   (release-deltas
    :initform nil
    :accessor project-release-deltas)
   (slots-changed
    :initform nil
    :accessor project-slots-changed))
  (:documentation
   "The base implementation of projects. Contains a mapping from version string
to release objects. When this mapping is accessed for the first time, all
release data from the projects releases file is loaded and release objects are
instantiated."))

(defmethod initialize-instance :after ((project project)
                                       &key releases
                                       &allow-other-keys)
  (let ((release-ht (make-hash-table :test 'equal)))
    (dolist (r releases)
      (setf (gethash r release-ht) nil))
    (setf (project-release-ht project) release-ht)))

(defmethod (setf project-repo) :after (new-value (project project))
  (pushnew 'repo (project-slots-changed project)))

(defmethod (setf project-version-scheme) :after (new-value (project project))
  (pushnew 'version-scheme (project-slots-changed project)))

(defmethod (setf project-releases-object-version) :after (new-value (project project))
  (pushnew 'releases-object-version (project-slots-changed project)))

(defmethod index-project-class (index)
  "Register this as the default project class."
  'project)

(defmethod project-object-path ((project project) suffix)
  (uiop:strcat "projects/"
               (project-name project)
               "/"
               suffix))

(defmethod project-add-release ((project project) new-release)
  (unless (eql (project new-release) project)
    (error "This release does not belong to this project!"))
  (setf (gethash (release-version new-release) (project-release-ht project)) new-release)
  (push (cons :add-release new-release) (project-release-deltas project))
  project)

(defmethod project-added-release-versions ((project project))
  (mapcar (compose #'release-version #'cdr)
          (remove-if-not (lambda (x) (eql (car x) :add-release)) (project-release-deltas project))))

(defmethod project-removed-release-versions ((project project))
  ;; We don't allow removing releases yet.
  nil)

(defmethod project-changes-appendable-p ((project project))
  (every (lambda (x)
           (eql (car x) :add-release))
         (project-release-deltas project)))

(defmethod project-dirty-p ((project project))
  (or
   (project-release-deltas project)
   (project-slots-changed project)))

(defmethod slot-unbound (class (project project) (slot-name (eql 'releases-object-version)))
  (with-open-object-stream (s (index-object-store (index project))
                              (project-object-path project "releases-object-version")
                              :if-does-not-exist nil)
    (setf (project-releases-object-version project)
          (when s (with-clpi-io-syntax () (read s))))))

(defmethod project-index-plist ((project project))
  (let ((out)
        (release-versions (hash-table-keys (project-release-ht project))))
    (when release-versions
      (push release-versions out)
      (push :releases out))
    out))

(defmethod project-load-releases-from-stream! ((project project) stream)
  (with-stream-forms (form stream)
    (destructuring-bind (version &rest plist)
        form
      (setf (gethash version (project-release-ht project))
            (apply #'make-instance
                   (project-release-class project)
                   :project project
                   :version version
                   plist)))))

(defmethod project-release ((project project) version &optional (error t))
  (unless (project-release-data-read-p project)
    (with-open-object-stream (stream (index-object-store (index project)) (project-releases-path project)
                                           :if-does-not-exist nil)
      (when stream
        (project-load-releases-from-stream! project stream))
      (setf (project-release-data-read-p project) t)))
  (multiple-value-bind (release exists-p)
      (gethash version (project-release-ht project))
    (cond
      ((and (not exists-p) (not error))
       (values nil nil))
      ((not exists-p)
       (error 'project-no-such-version :project project :version version))
      (release
       (values release t))
      (error
       (error 'project-missing-version :project project :version version))
      (t
       nil))))

(defmethod project-releases ((project project))
  ;; We can't call HASH-TABLE-VALUES directly because PROJECT-RELEASE contains
  ;; the logic necessary to do the loading from object and signaling.
  (mapcar (curry #'project-release project) (project-versions project)))

(defmethod project-releases-path ((project project))
  (let ((releases-object-version (project-releases-object-version project)))
    (if releases-object-version
        (project-object-path project (uiop:strcat "releases-" (write-to-string releases-object-version)))
        (project-object-path project (uiop:strcat "releases")))))

(defmethod project-save ((project project))
  (when (project-dirty-p project)
    (let ((appendable-p (project-changes-appendable-p project)))
      (when (and (project-releases-object-version project) (not appendable-p))
        (incf (project-releases-object-version project)))
      (with-open-object-stream (s (index-object-store (index project)) (project-releases-path project)
                                  :direction :output
                                  :if-exists (if appendable-p :append :supersede))
        (if appendable-p
            (project-write-changes-to-stream project s)
            (project-write-to-stream project s))))
    (awhen (and (member 'releases-object-version (project-slots-changed project))
                (project-releases-object-version project))
      (with-open-object-stream (s (index-object-store (index project))
                                  (project-object-path project "releases-object-version")
                                  :direction :output
                                  :if-exists :supersede)
        (with-clpi-io-syntax ()
          (write it :stream s)
          (terpri s)))))
  (when (or (member 'repo (project-slots-changed project))
            (and (slot-boundp project 'repo)
                 (not (probe-object (index-object-store (index project))
                                    (project-object-path project "repo")))))
    (with-open-object-stream (s (index-object-store (index project)) (project-object-path project "repo")
                                :direction :output
                                :if-exists :supersede)
      (with-clpi-io-syntax ()
        (write (repo-to-description (project-repo project)) :stream s)
        (terpri s))))
  (when (or (member 'version-scheme (project-slots-changed project))
            (and (slot-boundp project 'version-scheme)
                 (not (probe-object (index-object-store (index project))
                                    (project-object-path project "version-scheme")))))
    (with-open-object-stream (s (index-object-store (index project))
                                (project-object-path project "version-scheme")
                                :direction :output
                                :if-exists :supersede)
      (with-clpi-io-syntax ()
        (write (project-version-scheme project) :stream s)
        (terpri s))))
  (setf (project-slots-changed project) nil))

(defmethod project-versions ((project project))
  (hash-table-keys (project-release-ht project)))

(defmethod project-write-changes-to-stream ((project project) s)
  (assert (every (lambda (x) (eql (car x) :add-release)) (project-release-deltas project)))
  (let ((release-descs (mapcar (lambda (release)
                                 (list* (release-version release) (release-plist release)))
                               (mapcar #'cdr (project-release-deltas project))))
        (version-scheme (project-version-scheme project)))
    (when version-scheme
      (setf release-descs (sort release-descs (ecase version-scheme
                                                (:date
                                                 #'string<)
                                                (:semver
                                                 (lambda (x y)
                                                   (cl-semver:version<
                                                    (cl-semver:read-version-from-string x)
                                                    (cl-semver:read-version-from-string y)))))
                                :key #'car)))
    (with-clpi-io-syntax ()
      (dolist (release-desc release-descs)
        (write release-desc
               :stream s)
        (terpri s))))
  (setf (project-release-deltas project) nil))

(defmethod project-write-to-stream ((project project) s)
  (let ((release-descs (mapcar (lambda (release)
                                 (list* (release-version release) (release-plist release)))
                               (project-releases project)))
        (version-scheme (project-version-scheme project)))
    (when version-scheme
      (setf release-descs (sort release-descs (ecase version-scheme
                                                (:date
                                                 #'string<)
                                                (:semver
                                                 (lambda (x y)
                                                   (cl-semver:version<
                                                    (cl-semver:read-version-from-string x)
                                                    (cl-semver:read-version-from-string y)))))
                                :key #'car)))
    (with-clpi-io-syntax ()
      (dolist (release-desc release-descs)
        (write release-desc
               :stream s)
        (terpri s))))
  (setf (project-release-deltas project) nil))

(defmethod slot-unbound (class (project project) (slot-name (eql 'repo)))
  "Create the repository object on demand."
  (with-open-object-stream (s (index-object-store (index project)) (project-object-path project "repo")
                              :if-does-not-exist nil)
    (setf (slot-value project slot-name)
          (when s
            (make-repo-from-description (read s))))))

(defmethod slot-unbound (class (project project) (slot-name (eql 'version-scheme)))
  (with-open-object-stream (s (index-object-store (index project))
                              (project-object-path project "version-scheme")
                              :if-does-not-exist nil)
    (setf (slot-value project slot-name)
          (when s (with-clpi-io-syntax () (read s))))))
