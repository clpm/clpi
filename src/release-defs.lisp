;;;; Project release definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/release-defs
    (:use #:cl)
  (:export #:release
           #:release-add-system-file
           #:release-archive-type
           #:release-md5
           #:release-no-such-system-file
           #:release-no-such-system-file-namestring
           #:release-no-such-system-file-release
           #:release-no-such-system-release
           #:release-no-such-system-release-release
           #:release-no-such-system-release-system-name
           #:release-plist
           #:release-size
           #:release-system-file
           #:release-system-file-namestrings
           #:release-system-files
           #:release-system-names
           #:release-system-release
           #:release-system-releases
           #:release-url
           #:release-version))

(in-package #:clpi/release-defs)

(define-condition release-no-such-system-release (error)
  ((release
    :initarg :release
    :reader release-no-such-system-release-release)
   (system-name
    :initarg :system-name
    :reader release-no-such-system-release-system-name)))

(define-condition release-no-such-system-file (error)
  ((release
    :initarg :release
    :reader release-no-such-system-file-release)
   (namestring
    :initarg :namestring
    :reader release-no-such-system-file-namestring)))

(defgeneric release (object)
  (:documentation
   "Returns the release to which OBJECT belongs."))

(defgeneric release-add-system-file (release system-file)
  (:documentation
   "Add the SYSTEM-FILE to the RELEASE."))

(defgeneric release-archive-type (release)
  (:documentation
   "Return a keyword describing the archive type of RELEASE. Either :TAR.GZ or
:ZIP."))

(defgeneric release-md5 (release)
  (:documentation
   "The md5sum of the release archive or NIL."))

(defgeneric release-plist (release)
  (:documentation
   "Return a plist representing the RELEASE."))

(defgeneric release-size (release)
  (:documentation
   "The size of the release archive or NIL."))

(defgeneric release-system-file (release namestring &optional error)
  (:documentation
   "Return the system file in RELEASE with NAMESTRING.

If no such system file exists and ERROR is non-NIL (default) an error of type
RELEASE-NO-SUCH-SYSTEM-FILE is signaled. Otherwise, NIL is returned."))

(defgeneric release-system-files (release)
  (:documentation
   "Return a list of all system files in RELEASE."))

(defgeneric release-system-file-namestrings (release)
  (:documentation
   "Return a list containing the namestrings to all system files in RELEASE."))

(defgeneric release-system-names (release)
  (:documentation
   "Return a list containing the names of all systems defined in RELEASE."))

(defgeneric release-system-release (release system-name &optional error)
  (:documentation
   "Return the system release in RELEASE for the system named SYSTEM-NAME.

If ERROR is non-NIL (default), a RELEASE-NO-SUCH-SYSTEM-RELEASE error is
signaled if a system release for SYSTEM-NAME cannot be found. Otherwise, NIL is
returned."))

(defgeneric release-system-releases (release)
  (:documentation
   "Return a list of all system releases contained in RELEASE."))

(defgeneric release-url (release)
  (:documentation
   "Return the url where RELEASE is located."))

(defgeneric release-version (release)
  (:documentation
   "The version of RELEASE."))
