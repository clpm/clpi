;;;; Common index definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/index-defs
    (:use #:cl
          #:alexandria)
  (:export #:index
           #:index-add-project
           #:index-add-system
           #:index-initialize
           #:index-initialize-internal
           #:index-object-store
           #:index-plist
           #:index-project
           #:index-project-class
           #:index-project-index-object-path
           #:index-project-index-object-version
           #:index-project-missing
           #:index-project-missing-index
           #:index-project-missing-project-name
           #:index-project-names
           #:index-projects
           #:index-save
           #:index-sync
           #:index-system
           #:index-system-class
           #:index-system-file-class
           #:index-system-index-object-path
           #:index-system-index-object-version
           #:index-system-missing
           #:index-system-missing-index
           #:index-system-missing-system-name
           #:index-system-names
           #:index-system-release-class
           #:index-systems
           #:index-version))

(in-package #:clpi/index-defs)

(define-condition index-project-missing ()
  ((index
    :initarg :index
    :reader index-project-missing-index)
   (project-name
    :initarg :project-name
    :reader index-project-missing-project-name))
  (:documentation
   "Condition stating that a project is missing from an index."))

(define-condition index-system-missing ()
  ((index
    :initarg :index
    :reader index-system-missing-index)
   (system-name
    :initarg :system-name
    :reader index-system-missing-system-name))
  (:documentation
   "Condition stating that a system is missing from an index."))

(defgeneric index (object)
  (:documentation
   "Return the index to which a given OBJECT belongs."))

(defgeneric index-add-project (index project)
  (:documentation
   "Add the PROJECT to the INDEX."))

(defgeneric index-add-system (index system)
  (:documentation
   "Add the SYSTEM to the INDEX."))

(defgeneric index-initialize (index)
  (:documentation
   "Read the INDEX information from the index object and populate the
slots. This is not called as part of INITIALIZE-INSTANCE in order to allow work
to be defered until first access instead of instantiation."))

(defgeneric index-initialize-internal (index projects systems)
  (:documentation
   "Performs initialization of the index using the data read from the index
object."))

(defgeneric index-object-store (index)
  (:documentation
   "Returns the object store backing the index."))

(defgeneric index-plist (index)
  (:documentation
   "Return a plist description of INDEX suitable for writing to the \"index\"
object."))

(defgeneric index-project (index project-name &optional error)
  (:documentation
   "Return the project with the name PROJECT-NAME.

If ERROR is non-NIL (default), an error of type INDEX-PROJECT-MISSING is
signaled."))

(defgeneric index-project-class (index)
  (:documentation
   "Return the class used to instantiate projects for INDEX."))

(defgeneric index-project-index-object-path (index)
  (:documentation
   "Return the path of the INDEX's project-index object."))

(defgeneric index-project-index-object-version (index)
  (:documentation
   "Return the version of the INDEX's project-index object."))

(defgeneric index-project-names (index)
  (:documentation
   "Return a list of the names of projects in INDEX."))

(defgeneric index-projects (index)
  (:documentation
   "Return a list of all projects in INDEX."))

(defgeneric index-save (index)
  (:documentation
   "Save modifications to INDEX to persistent storage."))

(defgeneric index-sync (index)
  (:documentation
   "If INDEX is backed by a dual object store, sync all objects into the
secondary."))

(defgeneric index-system (index system-name &optional error)
  (:documentation
   "Return the system with the name SYSTEM-NAME.

If ERROR is non-NIL (default), an error of type INDEX-SYSTEM-MISSING is
signaled."))

(defgeneric index-system-class (index)
  (:documentation
   "Return the class used to instantiate systems for INDEX."))

(defgeneric index-system-file-class (index)
  (:documentation
   "Return the class used to instantiate system files for INDEX."))

(defgeneric index-system-index-object-path (index)
  (:documentation
   "Return the path of the INDEX's system-index object."))

(defgeneric index-system-index-object-version (index)
  (:documentation
   "Return the version of the INDEX's system-index object."))

(defgeneric index-system-names (index)
  (:documentation
   "Return a list of the names of systems in INDEX."))

(defgeneric index-system-release-class (index)
  (:documentation
   "Return the class used to instantiate system releases for INDEX."))

(defgeneric index-systems (index)
  (:documentation
   "Return a list of all systems from INDEX."))

(defgeneric index-version (index)
  (:documentation
   "Returns the CLPI version of INDEX."))
