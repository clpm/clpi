;;;; VCS Repositories
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/repos
    (:use #:cl
          #:clpi/repos/defs
          #:clpi/repos/github
          #:clpi/repos/gitlab)
  (:reexport #:clpi/repos/defs)
  (:reexport #:clpi/repos/github)
  (:reexport #:clpi/repos/gitlab)
  (:export #:make-repo-from-description))

(in-package #:clpi/repos)

(defun make-repo-from-description (desc)
  (destructuring-bind (repo-type &rest args) desc
    (ecase repo-type
      (:github
       (apply #'make-instance 'github-repo args))
      (:gitlab
       (apply #'make-instance 'gitlab-repo args)))))
