;;;; Utilities
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/utils
    (:use #:cl)
  (:export #:with-clpi-io-syntax
           #:with-stream-forms))

(in-package #:clpi/utils)

(defun call-with-clpi-io-syntax (thunk)
  (uiop:with-safe-io-syntax ()
    (let ((*print-case* :downcase))
      (funcall thunk))))

(defun call-with-stream-forms (stream thunk)
  (uiop:with-safe-io-syntax ()
    (loop
      :with eof := '#:eof
      :for form := (read stream nil eof)
      :until (eql form eof)
      :do
         (funcall thunk form))))

(defmacro with-clpi-io-syntax (() &body body)
  `(call-with-clpi-io-syntax (lambda () ,@body)))

(defmacro with-stream-forms ((form-name stream) &body body)
  `(call-with-stream-forms ,stream (lambda (,form-name) ,@body)))
