;;;; System release implementation
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/system-release
    (:use #:cl
          #:anaphora
          #:clpi/defs)
  (:export #:system-release))

(in-package #:clpi/system-release)

(defclass system-release ()
  ((name
    :initarg :name
    :reader system-release-system-name)
   (system-file
    :initarg :system-file
    :reader system-release-system-file)
   (version
    :initarg :version
    :initform nil
    :reader system-release-version)
   (description
    :initarg :description
    :initform nil
    :reader system-release-description)
   (license
    :initarg :license
    :initform nil
    :reader system-release-license)
   (dependencies
    :initarg :dependencies
    :initform nil
    :reader system-release-dependencies)))

(defmethod index ((system-release system-release))
  (index (system-release-system-file system-release)))

(defmethod project ((system-release system-release))
  (project (system-release-system-file system-release)))

(defmethod release ((system-release system-release))
  (release (system-release-system-file system-release)))

(defmethod system ((system-release system-release))
  (index-system (index system-release) (system-release-system-name system-release)))

(defmethod system-release-plist ((system-release system-release))
  (append
   (awhen (system-release-version system-release)
     (list :version it))
   (awhen (system-release-license system-release)
     (list :license it))
   (awhen (system-release-description system-release)
     (list :description it))
   (awhen (system-release-dependencies system-release)
     (list :dependencies it))))

(defmethod index-system-release-class (system)
  'system-release)
