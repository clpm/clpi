;;;; System release definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/system-release-defs
    (:use #:cl)
  (:export #:system-release-dependencies
           #:system-release-description
           #:system-release-license
           #:system-release-plist
           #:system-release-system-file
           #:system-release-system-name
           #:system-release-version))

(in-package #:clpi/system-release-defs)

(defgeneric system-release-dependencies (system-release)
  (:documentation
   "Return the dependencies of SYSTEM-RELEASE."))

(defgeneric system-release-description (system-release)
  (:documentation
   "Return the description of the SYSTEM-RELEASE."))

(defgeneric system-release-license (system-release)
  (:documentation
   "Return the license of the SYSTEM-RELEASE."))

(defgeneric system-release-plist (system-release)
  (:documentation
   "Return a plist descrbing the SYSTEM-RELEASE."))

(defgeneric system-release-system-file (system-release)
  (:documentation
   "Returns the system file instance that defines this SYSTEM-RELEASE."))

(defgeneric system-release-system-name (system-release)
  (:documentation
   "Return a string naming the system of SYSTEM-RELEASE."))

(defgeneric system-release-version (system-release)
  (:documentation
   "The version of the system in this release."))
