;;;; System definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/system-defs
    (:use #:cl)
  (:export #:system
           #:system-add-system-release
           #:system-added-system-release-descriptions
           #:system-index-plist
           #:system-name
           #:system-no-such-version
           #:system-no-such-version-system
           #:system-no-such-version-version
           #:system-object-path
           #:system-primary-project
           #:system-save
           #:system-system-release-descriptions
           #:system-releases
           #:system-removed-system-release-descriptions
           #:system-version-release-names
           #:system-version-releases
           #:system-versions))

(in-package #:clpi/system-defs)

(define-condition system-no-such-version (error)
  ((system
    :initarg :system
    :reader system-no-such-version-system)
   (version
    :initarg :version
    :reader system-no-such-version-version)))

(defgeneric system (object)
  (:documentation
   "Returns the system to which the object belongs."))

(defgeneric system-add-system-release (system new-system-release)
  (:documentation
   "Add a new system release to SYSTEM."))

(defgeneric system-added-system-release-descriptions (system)
  (:documentation
   "Returns a list of system release descriptions naming new versions of the
SYSTEM added since it was last saved."))

(defgeneric system-index-plist (system)
  (:documentation
   "Return a plist for describing the SYSTEM in an index."))

(defgeneric system-name (system)
  (:documentation
   "The name of SYSTEM."))

(defgeneric system-object-path (system suffix))

(defgeneric system-primary-project (system)
  (:documentation
   "Returns a string naming the primary project for SYSTEM."))

(defgeneric system-releases (system &optional error)
  (:documentation
   "A list of releases where this system is included.

If ERROR is non-NIL (default), a PROJECT-MISSING-VERSION condition is signaled
if a release object cannot be instantiated due to missing information in the
index. Otherwise, then such releases are silently dropped."))

(defgeneric system-removed-system-release-descriptions (system)
  (:documentation
   "Returns a list of system release descriptions naming versions of the SYSTEM
removed since it was last saved."))

(defgeneric system-save (system)
  (:documentation
   "Save the SYSTEM's data to objects in the index."))

(defgeneric system-system-release-descriptions (system)
  (:documentation
   "Returns a list describing all the system releases of SYSTEM."))

(defgeneric system-version-release-names (system version &optional error)
  (:documentation
   "Return a list of release names that provide VERSION of SYSTEM. A release
name is a two element list where the first is the name of a project and the
second is the version of that project.

If ERROR is non-NIL (default), a condition of type SYSTEM-NO-SUCH-VERSION is
signaled if the VERSOIN does not exist.

If ERROR is NIL, then NIL is returned from this function."))

(defgeneric system-version-releases (system version &key ignore-missing-releases
                                                      error)
  (:documentation
   "Return a list of release objects that provide VERSION of SYSTEM.

If ERROR is non-NIL (default), a SYSTEM-NO-SUCH-VERSION error is signaled if the
version does not exist. If ERROR is NIL, then NIL is returned if the VERSION
does not exist.

If IGNORE-MISSING-RELEASES is NIL (default), a PROJECT-MISSING-VERSION condition
is signaled if the release object cannot be instantiated due to missing
information in the index. Otherwise, then such releases are silently dropped."))

(defgeneric system-versions (system)
  (:documentation
   "Return a list with the versions of all released systems."))
