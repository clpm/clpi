;;;; System file implementation
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/system-file
    (:use #:cl
          #:alexandria
          #:clpi/defs)
  (:export #:system-file))

(in-package #:clpi/system-file)

(defclass system-file ()
  ((release
    :initarg :release
    :reader release)
   (enough-namestring
    :initarg :enough-namestring
    :reader system-file-enough-namestring)
   (system-release-ht
    :reader system-file-system-release-ht)))

(defmethod initialize-instance :after ((system-file system-file)
                                       &key systems &allow-other-keys)
  (let ((ht (make-hash-table :test 'equal)))
    (dolist (desc systems)
      (destructuring-bind (system-name &rest plist)
          desc
        (setf (gethash system-name ht)
              (apply #'make-instance
                     (index-system-release-class (index system-file))
                     :system-file system-file
                     :name system-name
                     plist))))
    (setf (slot-value system-file 'system-release-ht) ht)))

(defmethod index ((system-file system-file))
  (index (release system-file)))

(defmethod index-system-file-class (index)
  "Register this as the default system file implementation."
  'system-file)

(defmethod project ((system-file system-file))
  (project (release system-file)))

(defmethod system-file-add-system-release ((system-file system-file) system-release)
  (setf (gethash (system-release-system-name system-release)
                 (system-file-system-release-ht system-file))
        system-release))

(defmethod system-file-alist ((system-file system-file))
  (mapcar (lambda (sr)
            (list* (system-release-system-name sr)
                   (system-release-plist sr)))
          (system-file-system-releases system-file)))

(defmethod system-file-system-names ((system-file system-file))
  (mapcar #'system-release-system-name (system-file-system-releases system-file)))

(defmethod system-file-system-release ((system-file system-file) system-name &optional error)
  (multiple-value-bind (system-release exists-p)
      (gethash system-name (system-file-system-release-ht system-file))
    (cond
      (exists-p
       system-release)
      (error
       (error 'system-file-no-such-system :system-name system-name :system-file system-file))
      (t
       nil))))

(defmethod system-file-system-releases ((system-file system-file))
  (hash-table-values (system-file-system-release-ht system-file)))
