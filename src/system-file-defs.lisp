;;;; System file definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/system-file-defs
    (:use #:cl)
  (:export #:system-file-add-system-release
           #:system-file-enough-namestring
           #:system-file-no-such-system
           #:system-file-no-such-system-system-file
           #:system-file-no-such-system-name
           #:system-file-alist
           #:system-file-system-names
           #:system-file-system-release
           #:system-file-system-releases))

(in-package #:clpi/system-file-defs)

(define-condition system-file-no-such-system (error)
  ((system-file
    :initarg :system-file
    :reader system-file-no-such-system-system-file)
   (system-name
    :initarg :system-name
    :reader system-file-no-such-system-system-name)))

(defgeneric system-file-add-system-release (system-file system-release)
  (:documentation
   "Add a system release to this system file."))

(defgeneric system-file-enough-namestring (system-file)
  (:documentation
   "Return a namestring suitable for locating the system file in its release."))

(defgeneric system-file-alist (system-file)
  (:documentation
   "Return an alist representing the SYSTEM-FILE."))

(defgeneric system-file-system-names (system-file)
  (:documentation
   "Return a list with the names of all systems defined within SYSTEM-FILE."))

(defgeneric system-file-system-release (system-file system-name &optional error)
  (:documentation
   "Return the system release for SYSTEM-NAME, defined within SYSTEM-FILE.

If the system does not exist in system file and ERROR is non-NIL (default), an
error of type SYSTEM-FILE-NO-SUCH-SYSTEM is signaled. Otherwise NIL is
returned."))

(defgeneric system-file-system-releases (system-file)
  (:documentation
   "Return a list of all system releases defined within SYSTEM-FILE."))
