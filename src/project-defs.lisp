;;;; Project definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/project-defs
    (:use #:cl)
  (:export #:project
           #:project-add-release
           #:project-added-release-versions
           #:project-changes-appendable-p
           #:project-commit-new-releases-to-stream!
           #:project-dirty-p
           #:project-releases-object-version
           #:project-index-plist
           #:project-load-releases-from-stream!
           #:project-missing-version
           #:project-missing-version-project
           #:project-missing-version-version
           #:project-name
           #:project-no-such-version
           #:project-no-such-version-project
           #:project-no-such-version-version
           #:project-object-path
           #:project-release
           #:project-release-class
           #:project-releases
           #:project-releases-path
           #:project-removed-release-versions
           #:project-repo
           #:project-save
           #:project-version-scheme
           #:project-versions
           #:project-write-changes-to-stream
           #:project-write-to-stream))

(in-package #:clpi/project-defs)

(define-condition project-missing-version (error)
  ((project
    :initarg :project
    :reader project-missing-version-project)
   (version
    :initarg :version
    :reader project-missing-version-version)))

(define-condition project-no-such-version (error)
  ((project
    :initarg :project
    :reader project-no-such-version-project)
   (version
    :initarg :version
    :reader project-no-such-version-version)))

(defgeneric project (object)
  (:documentation
   "Returns the project to which the object belongs."))

(defgeneric project-add-release (project new-release)
  (:documentation
   "Add a new release to PROJECT."))

(defgeneric project-added-release-versions (project)
  (:documentation
   "Returns a list of strings naming new versions of the PROJECT added since it
was last saved."))

(defgeneric project-changes-appendable-p (project)
  (:documentation
   "Returns a boolean indicating if the changes made to the project can be
appended to the existing object."))

(defgeneric project-dirty-p (project)
  (:documentation
   "Returns a boolean indicating if the project has changes that can be
committed to the backing objects."))

(defgeneric project-releases-object-version (project)
  (:documentation
   "Return the current releases-object-version of the PROJECT."))

(defgeneric project-index-plist (project)
  (:documentation
   "Return a plist for describing the PROJECT in an index."))

(defgeneric project-load-releases-from-stream! (project stream)
  (:documentation
   "Load all project releases from STREAM into PROJECT. If a release in STREAM
is already present in PROJECT, it is silently skipped."))

(defgeneric project-name (project)
  (:documentation
   "Return the name of PROJECT."))

(defgeneric project-object-path (project suffix))

(defgeneric project-release (project version-string &optional error)
  (:documentation
   "Returns the release object matching the given version string.

If ERROR is T (default) and the release cannot be found (there is no such
version or the index does not contain the information about this version) this
function signals an error. If the version is valid (and the data is missing from
the index), a condition of PROJECT-MISSING-VERSION is signaled. If the version
is invalid, a condition of PROJECT-NO-SUCH-VERSION is signaled.

If ERROR is NIL and the release cannot be found, the first return value is
NIL."))

(defgeneric project-releases (project)
  (:documentation
   "Return all releases of PROJECT."))

(defgeneric project-release-class (project)
  (:documentation
   "Return the class used to instantiate releases for PROJECT."))

(defgeneric project-releases-path (project)
  (:documentation
   "Returns the path to the PROJECT's releases object in its index."))

(defgeneric project-removed-release-versions (project)
  (:documentation
   "Returns a list of strings naming versions of the PROJECT removed since it
was last saved."))

(defgeneric project-repo (project)
  (:documentation
   "Returns the source repository for PROJECT."))

(defgeneric project-save (project)
  (:documentation
   "Save the PROJECT to objects in the index."))

(defgeneric project-version-scheme (project)
  (:documentation
   "Returns a keyword describing how version numbers should be compared between
releases."))

(defgeneric project-versions (project)
  (:documentation
   "Return the version string for every release of PROJECT.

NOTE: If a version string is returned by this function, it *does not* guarantee
that PROJECT-RELEASE will be able to return the release object corresponding to
that version. See PROJECT-RELEASE for more information."))

(defgeneric project-write-changes-to-stream (project stream)
  (:documentation
   "Write the PROJECT's changes to STREAM."))

(defgeneric project-write-to-stream (project stream)
  (:documentation
   "Write the project's release data to STREAM."))
