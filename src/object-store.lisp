;;;; Base object store implementation
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/object-store
    (:use #:cl)
  (:export #:object-store))

(in-package #:clpi/object-store)

(defclass object-store ()
  ()
  (:documentation
   "The base class for an object store."))
