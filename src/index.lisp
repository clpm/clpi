;;;; Base index implementation
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/index
    (:use #:cl
          #:alexandria
          #:anaphora
          #:clpi/defs
          #:clpi/object-store-dual
          #:clpi/project
          #:clpi/system
          #:clpi/utils)
  (:export #:index))

(in-package #:clpi/index)

(defclass index ()
  ((object-store
    :initarg :object-store
    :accessor index-object-store)
   (project-ht
    :accessor index-project-ht
    :documentation
    "Maps project names to project instances.")
   (version
    :accessor index-version
    :documentation
    "The version of the CLPI spec to which this index adhears.")
   (project-index-object-version
    :accessor index-project-index-object-version
    :documentation
    "The version of the project-index object.")
   (system-index-object-version
    :accessor index-system-index-object-version
    :documentation
    "The version of the system-index object.")
   (system-ht
    :accessor index-system-ht
    :documentation
    "Maps system names to system instances.")
   (new-projects
    :initform nil
    :accessor index-new-projects
    :documentation
    "A list of new projects added to the index.")
   (new-systems
    :initform nil
    :accessor index-new-systems
    :documentation
    "A list of new systems added to the index."))
  (:documentation
   "The base class for an index. Contains a mapping from project (system) names
to projects (systems). Each map slot starts as unbound, when first accessed, the
project-index and system-index files are read to instantiate the objects."))

(defmethod index ((index index))
  index)

(defmethod index-add-project ((index index) project)
  (multiple-value-bind (existing-project exists-p)
      (gethash (project-name project) (index-project-ht index))
    (when (and exists-p (not (eql project existing-project)))
      (error "Project already exists!"))
    (unless exists-p
      (push project (index-new-projects index))
      (setf (gethash (project-name project) (index-project-ht index)) project)))
  index)

(defmethod index-add-system ((index index) system)
  (multiple-value-bind (existing-system exists-p)
      (gethash (system-name system) (index-system-ht index))
    (when (and exists-p (not (eql system existing-system)))
      (error "System already exists!"))
    (unless exists-p
      (push system (index-new-systems index))
      (setf (gethash (system-name system) (index-system-ht index)) system)))
  index)

(defmethod index-project-index-object-path ((index index))
  (let ((version (index-project-index-object-version index)))
    (if version
        (uiop:strcat "project-index-" (write-to-string version))
        "project-index")))

(defmethod index-system-index-object-path ((index index))
  (let ((version (index-system-index-object-version index)))
    (if version
        (uiop:strcat "system-index-" (write-to-string version))
        "system-index")))

(defmethod index-initialize ((index index))
  (let ((projects (make-hash-table :test 'equal))
        (systems (make-hash-table :test 'equal)))
    (with-open-object-stream (s (index-object-store index) "clpi-version" :if-does-not-exist nil)
      (if s
          (with-clpi-io-syntax ()
            (setf (index-version index) (read s)))
          (setf (index-version index) "0.4")))
    (with-open-object-stream (s (index-object-store index) "project-index-object-version"
                                :if-does-not-exist nil)
      (setf (index-project-index-object-version index) (when s (with-clpi-io-syntax () (read s)))))
    (with-open-object-stream (s (index-object-store index) "system-index-object-version"
                                :if-does-not-exist nil)
      (setf (index-system-index-object-version index) (when s (with-clpi-io-syntax () (read s)))))
    (with-open-object-stream (s (index-object-store index) (index-project-index-object-path index)
                                :if-does-not-exist nil)
      (when s
        (with-stream-forms (f s)
          (destructuring-bind (project-name &rest versions)
              f
            (setf (gethash project-name projects) (append versions (gethash project-name projects)))))))
    (with-open-object-stream (s (index-object-store index) (index-system-index-object-path index)
                                :if-does-not-exist nil)
      (when s
        (with-stream-forms (f s)
          (destructuring-bind (system-name &rest versions)
              f
            (setf (gethash system-name systems) (append versions (gethash system-name systems)))))))
    (index-initialize-internal index projects systems)))

(defmethod index-initialize-internal ((index index) projects systems)
  (let ((project-ht (make-hash-table :test 'equal))
        (system-ht (make-hash-table :test 'equal)))
    (maphash (lambda (project-name project-versions)
               (setf (gethash project-name project-ht)
                     (make-instance (index-project-class index)
                                    :index index
                                    :name project-name
                                    :releases project-versions)))
             projects)
    (maphash (lambda (system-name system-versions)
               (setf (gethash system-name system-ht)
                     (make-instance (index-system-class index)
                                    :index index
                                    :name system-name
                                    :releases system-versions)))
             systems)
    (setf (slot-value index 'project-ht) project-ht
          (slot-value index 'system-ht) system-ht)))

(defmethod index-plist ((index index))
  (let ((project-alist)
        (system-alist))
    (dolist (name (index-project-names index))
      (push (cons name (project-index-plist (index-project index name))) project-alist))
    (setf project-alist (sort project-alist #'string< :key #'car))
    (dolist (name (index-system-names index))
      (push (cons name (system-index-plist (index-system index name))) system-alist))
    (setf system-alist (sort system-alist #'string< :key #'car))
    (list :projects project-alist
          :systems system-alist)))

(defmethod index-project ((index index) project-name &optional (error t))
  (multiple-value-bind (value exists-p)
      (gethash project-name (index-project-ht index))
    (unless (or exists-p (not error))
      (error 'index-project-missing :index index :project-name project-name))
    value))

(defmethod index-project-names ((index index))
  (hash-table-keys (index-project-ht index)))

(defmethod index-projects ((index index))
  (hash-table-values (index-project-ht index)))

(defmethod index-save ((index index))
  (let ((project-added-release-versions-ht (make-hash-table))
        (project-removed-release-versions-ht (make-hash-table))
        project-index-object-version-bumped
        (system-added-release-versions-ht (make-hash-table))
        (system-removed-release-versions-ht (make-hash-table))
        system-index-object-version-bumped)

    (dolist (p (index-projects index))
      ;; First, figure out the added and removed project releases.
      (awhen (project-added-release-versions p)
        (setf (gethash p project-added-release-versions-ht) it))
      (awhen (project-removed-release-versions p)
        (setf (gethash p project-removed-release-versions-ht) it))
      ;; Then write to disk.
      (project-save p))
    (dolist (s (index-systems index))
      ;; First, figure out the added and removed system releases.
      (awhen (system-added-system-release-descriptions s)
        (setf (gethash s system-added-release-versions-ht) it))
      (awhen (system-removed-system-release-descriptions s)
        (setf (gethash s system-removed-release-versions-ht) it))
      ;; Then write to disk.
      (system-save s))
    (with-open-object-stream (s (index-object-store index) "clpi-version"
                                :direction :output
                                :if-exists :supersede)
      (with-clpi-io-syntax ()
        (write "0.4" :stream s)
        (terpri s)))

    ;; Determine if the project-index version needs to be updated.
    (when (and (index-project-index-object-version index)
               (not (zerop (hash-table-count project-removed-release-versions-ht))))
      (incf (index-project-index-object-version index))
      (setf project-index-object-version-bumped t))
    ;; Determine if the project-index version needs to be updated.
    (when (and (index-system-index-object-version index)
               (not (zerop (hash-table-count system-removed-release-versions-ht))))
      (incf (index-system-index-object-version index))
      (setf system-index-object-version-bumped t))

    ;; Write the project-index. If we bumped the version or the version is NIL,
    ;; we need to write the whole file.
    (if (or (null (index-project-index-object-version index))
            project-index-object-version-bumped)
        (with-open-object-stream (s (index-object-store index) (index-project-index-object-path index)
                                    :direction :output
                                    :if-exists :supersede)
          (dolist (p (index-projects index))
            (with-clpi-io-syntax ()
              (write (list* (project-name p) (project-versions p)) :stream s)
              (terpri s))))
        (with-open-object-stream (s (index-object-store index) (index-project-index-object-path index)
                                    :direction :output
                                    :if-exists :append)
          (dolist (p (index-new-projects index))
            (with-clpi-io-syntax ()
              (write (list* (project-name p) (project-versions p)) :stream s)
              (terpri s))
            (remhash p project-added-release-versions-ht))
          (maphash (lambda (project added-versions)
                     (with-clpi-io-syntax ()
                       (write (list* (project-name project) added-versions) :stream s)
                       (terpri s)))
                   project-added-release-versions-ht)))
    (setf (index-new-projects index) nil)
    (awhen (index-project-index-object-version index)
      (with-open-object-stream (s (index-object-store index) "project-index-object-version"
                                  :direction :output
                                  :if-exists :supersede)
        (write it :stream s)))

    ;; Write the system-index. If we bumped the version or the version is NIL,
    ;; we need to write the whole file.
    (if (or (null (index-system-index-object-version index))
            system-index-object-version-bumped)
        (with-open-object-stream (s (index-object-store index) (index-system-index-object-path index)
                                    :direction :output
                                    :if-exists :supersede)
          (dolist (system (index-systems index))
            (with-clpi-io-syntax ()
              (write (list* (system-name system) (system-system-release-descriptions system)) :stream s)
              (terpri s))))
        (with-open-object-stream (s (index-object-store index) (index-system-index-object-path index)
                                    :direction :output
                                    :if-exists :append)
          (dolist (system (index-new-systems index))
            (with-clpi-io-syntax ()
              (write (list* (system-name system) (system-system-release-descriptions system)) :stream s)
              (terpri s))
            (remhash system system-added-release-versions-ht))
          (maphash (lambda (system added-versions)
                     (with-clpi-io-syntax ()
                       (write (list* (system-name system) added-versions) :stream s)
                       (terpri s)))
                   system-added-release-versions-ht)))
    (setf (index-new-systems index) nil)
    (awhen (index-system-index-object-version index)
      (with-open-object-stream (s (index-object-store index) "system-index-object-version"
                                  :direction :output
                                  :if-exists :supersede)
        (write it :stream s))))
  (values))

(defmethod index-sync ((index index))
  (let ((object-store (index-object-store index)))
    (when (typep object-store 'dual-object-store)
      (dual-object-store-update-object object-store "clpi-version")
      (dual-object-store-update-object object-store "project-index-object-version")
      (dual-object-store-update-object object-store "system-index-object-version")
      (dual-object-store-update-object object-store (index-project-index-object-path index))
      (dual-object-store-update-object object-store (index-system-index-object-path index))
      (dolist (p (index-projects index))
        (dual-object-store-update-object object-store (project-object-path p "repo"))
        (dual-object-store-update-object object-store (project-object-path p "authors"))
        (dual-object-store-update-object object-store (project-object-path p "maintainers"))
        (dual-object-store-update-object object-store (project-object-path p "homepage"))
        (dual-object-store-update-object object-store (project-object-path p "releases-object-version"))
        (dual-object-store-update-object object-store (project-object-path p "version-scheme"))
        (dual-object-store-update-object object-store (project-releases-path p)))
      (dolist (s (index-systems index))
        (dual-object-store-update-object object-store (system-object-path s "primary-project"))))))

(defmethod index-system ((index index) system-name &optional (error t))
  (multiple-value-bind (value exists-p)
      (gethash system-name (index-system-ht index))
    (unless (or exists-p (not error))
      (error 'index-system-missing :index index :system-name system-name))
    value))

(defmethod index-system-names ((index index))
  (hash-table-keys (index-system-ht index)))

(defmethod index-systems ((index index))
  (hash-table-values (index-system-ht index)))

(defmethod slot-unbound (class (index index) slot-name)
  (index-initialize index)
  (if (slot-boundp index slot-name)
      (slot-value index slot-name)
      (call-next-method)))
