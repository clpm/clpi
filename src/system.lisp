;;;; Base system implementation
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/system
    (:use #:cl
          #:alexandria
          #:clpi/defs
          #:clpi/utils)
  (:export #:system))

(in-package #:clpi/system)

(defclass system ()
  ((index
    :initarg :index
    :reader index)
   (name
    :initarg :name
    :reader system-name)

   (primary-project
    :initarg :primary-project
    :accessor system-primary-project)

   (release-version-ht
    :reader system-release-version-ht
    :documentation
    "Maps two element lists (project-name version) to the version of the system
contained in that release.")
   (version-release-ht
    :reader system-version-release-ht
    :documentation
    "Maps system versions to a list of releases (as two element lists) that
provide that version.")
   (system-release-deltas
    :initform nil
    :accessor system-system-release-deltas)))

(defmethod initialize-instance :after ((system system) &key releases &allow-other-keys)
  (let ((release-version-ht (make-hash-table :test 'equal))
        (version-release-ht (make-hash-table :test 'equal)))
    (dolist (r releases)
      (destructuring-bind (release-pair &optional system-version)
          r
        (setf (gethash release-pair release-version-ht) system-version)
        (push release-pair (gethash system-version version-release-ht))))
    (setf (slot-value system 'release-version-ht) release-version-ht
          (slot-value system 'version-release-ht) version-release-ht)))

(defmethod index-system-class (index)
  'system)

(defmethod system-object-path ((system system) path)
  (uiop:strcat "systems/"
               (asdf:primary-system-name (system-name system))
               "/" path))

(defmethod system-add-system-release ((system system) new-system-release)
  (unless (eql (system new-system-release) system)
    (error "This system-release does not belong to this system!"))
  (let ((release-spec (list (project-name (project new-system-release))
                            (release-version (release new-system-release)))))
    (setf (gethash release-spec
                   (system-release-version-ht system))
          (system-release-version new-system-release))
    (setf (gethash (system-release-version new-system-release)
                   (system-version-release-ht system))
          release-spec)
    (push (cons :add (if (system-release-version new-system-release)
                         (list release-spec (system-release-version new-system-release))
                         (list release-spec)))
          (system-system-release-deltas system)))
  system)

(defmethod system-added-system-release-descriptions ((system system))
  (mapcar #'cdr
          (remove-if-not (lambda (x) (eql (car x) :add)) (system-system-release-deltas system))))

(defmethod system-removed-system-release-descriptions ((system system))
  ;; We don't support removing system releases yet.
  nil)

(defmethod system-system-release-descriptions ((system system))
  (let (out)
    (maphash (lambda (k v)
               (push (if v (list k v) (list k)) out))
             (system-release-version-ht system))
    out))

(defmethod system-index-plist ((system system))
  (let ((out nil)
        system-release-versions)
    (maphash (lambda (k v)
               (push (if v (list k v) (list k)) system-release-versions))
             (system-release-version-ht system))
    (when system-release-versions
      (push :releases out)
      (push system-release-versions out))
    (nreverse out)))

(defmethod slot-unbound (class (system system) (slot-name (eql 'primary-project)))
  (with-open-object-stream (s (index-object-store (index system)) (system-object-path system "primary-project"))
    (setf (slot-value system slot-name)
          (when s (with-clpi-io-syntax () (read s))))))

(defmethod system-releases ((system system) &optional (error t))
  (loop
    :for (project-name project-version) :in (hash-table-keys (system-release-version-ht system))
    :for release := (project-release (index-project (index system) project-name) project-version error)
    :when release :collect release))

(defmethod system-save ((system system))
  (when (slot-boundp system 'primary-project)
    (with-open-object-stream (s (index-object-store (index system))
                                (system-object-path system "primary-project")
                                :direction :output
                                :if-exists :supersede)
      (with-clpi-io-syntax ()
        (write (system-primary-project system) :stream s)
        (terpri s))))
  (setf (system-system-release-deltas system) nil))

(defmethod system-version-release-names ((system system) version &optional (error t))
  (multiple-value-bind (names exists-p) (gethash version (system-version-release-ht system))
    (cond
      (exists-p
       names)
      (error
       (error 'system-no-such-version :system system :version version))
      (t
       nil))))

(defmethod system-version-releases ((system system) version &key ignore-missing-releases (error t))
  (let ((names (system-version-release-names system version error)))
    (remove nil (mapcar (lambda (name)
                          (destructuring-bind (project-name project-version)
                              name
                            (project-release (index-project (index system) project-name)
                                             project-version (not ignore-missing-releases))))
                        names))))

(defmethod system-versions ((system system))
  (hash-table-keys (system-version-release-ht system)))
