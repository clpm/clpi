;;;; Project release implementation
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/release
    (:use #:cl
          #:alexandria
          #:anaphora
          #:clpi/defs)
  (:export #:release))

(in-package #:clpi/release)

(defclass release ()
  ((project
    :initarg :project
    :reader project)
   (version
    :initarg :version
    :reader release-version)
   (system-file-ht
    :reader release-system-file-ht)
   (archive-type
    :initarg :archive-type
    :initform :tar.gz
    :reader release-archive-type)
   (systems
    :initarg :systems
    :reader release-systems)
   (url
    :initarg :url
    :reader release-url)
   (md5
    :initarg :md5
    :initform nil
    :reader release-md5)
   (size
    :initarg :size
    :initform nil
    :reader release-size)))

(defmethod initialize-instance :after ((release release)
                                       &key systems
                                       &allow-other-keys)
  (let ((system-file-ht (make-hash-table :test 'equal)))
    (dolist (desc systems)
      (destructuring-bind (enough-namestring . systems)
          desc
        (setf (gethash enough-namestring system-file-ht)
              (make-instance (index-system-file-class (index release))
                             :release release
                             :systems systems
                             :enough-namestring enough-namestring))))
    (setf (slot-value release 'system-file-ht) system-file-ht)))

(defmethod index ((release release))
  (index (project release)))

(defmethod project-release-class (project)
  "Register this as the default release implementation."
  'release)

(defmethod release-add-system-file ((release release) system-file)
  (setf (gethash (system-file-enough-namestring system-file) (release-system-file-ht release))
        system-file))

(defmethod release-plist ((release release))
  (append
   (list :url (release-url release))
   (awhen (release-size release)
     (list :size it))
   (awhen (release-md5 release)
     (list :md5 it))
   (let ((archive-type (release-archive-type release)))
     (unless (eql archive-type :tar.gz)
       (list :archive-type archive-type)))
   (list :systems (mapcar (lambda (x)
                            (list* (system-file-enough-namestring x)
                                   (system-file-alist x)))
                          (release-system-files release)))))

(defmethod release-system-file ((release release) namestring &optional (error t))
  (multiple-value-bind (system-file exists-p)
      (gethash namestring (release-system-file-ht release))
    (cond
      (exists-p
       system-file)
      (error
       (error 'release-no-such-system-file
              :namestring namestring
              :release release))
      (t
       nil))))

(defmethod release-system-file-namestrings ((release release))
  (hash-table-keys (release-system-file-ht release)))

(defmethod release-system-files ((release release))
  (hash-table-values (release-system-file-ht release)))

(defmethod release-system-names ((release release))
  (flatten (mapcar #'system-file-system-names (release-system-files release))))

(defmethod release-system-release ((release release) system-name &optional (error t))
  (loop
    :for system-file :in (release-system-files release)
    :for system-release := (system-file-system-release system-file system-name nil)
    :when system-release
      :do (return system-release)
    :finally
       (when error
         (error 'release-no-such-system-release :release release :system-name system-name))))

(defmethod release-system-releases ((release release))
  (flatten (mapcar #'system-file-system-releases (release-system-files release))))
