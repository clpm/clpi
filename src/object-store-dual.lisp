;;;; Dual Object Store
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/object-store-dual
    (:use #:cl
          #:alexandria
          #:clpi/defs
          #:clpi/object-store)
  (:import-from #:cl-ppcre)
  (:export #:dual-object-store
           #:dual-object-store-sync
           #:dual-object-store-update-object))

(in-package #:clpi/object-store-dual)

(defclass dual-object-store (object-store)
  ((primary
    :initarg :primary
    :reader dual-object-store-primary)
   (secondary
    :initarg :secondary
    :reader dual-object-store-secondary)
   (updated-paths
    :initform (make-hash-table :test 'equal)
    :reader dual-object-store-updated-paths))
  (:documentation
   "A logical view of an object store that is backed by two separate object
stores, the primary and the secondary. On the first access to any object in the
store, the secondary is updated from the primary and then the secondary is read."))

(defun path-already-updated-p (object-store path)
  (gethash path (dual-object-store-updated-paths object-store)))

(defun mark-path-updated (object-store path)
  (setf (gethash path (dual-object-store-updated-paths object-store)) t))

(defun path-appendable-p (path)
  (or (cl-ppcre:scan "^projects/[^/]*/releases-\\d+$" path)
      (cl-ppcre:scan "^project-index-\\d+$" path)
      (cl-ppcre:scan "^system-index-\\d+$" path)))

(defun dual-object-store-update-object (object-store path)
  (let* ((primary (dual-object-store-primary object-store))
         (secondary (dual-object-store-secondary object-store)))
    (if (path-appendable-p path)
        (with-open-object-stream (primary-stream primary path
                                                 :binary-p t
                                                 :offset (object-size secondary path)
                                                 :if-does-not-exist nil)
          (when primary-stream
            (object-append secondary path primary-stream)))
        (with-open-object-stream (s primary path
                                    :if-does-not-exist nil)
          (when s
            (object-write secondary path s))))
    (mark-path-updated object-store path)))

(defmethod open-object-stream ((object-store dual-object-store) path (direction (eql :input))
                               &key binary-p offset
                                 (if-does-not-exist :error) if-exists)
  (unless (path-already-updated-p object-store path)
    (dual-object-store-update-object object-store path))
  (open-object-stream (dual-object-store-secondary object-store) path direction
                      :offset offset :binary-p binary-p
                      :if-does-not-exist if-does-not-exist :if-exists if-exists))
