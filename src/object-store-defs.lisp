;;;; Object store definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/object-store-defs
    (:use #:cl
          #:alexandria)
  (:export #:call-with-open-object-stream
           #:object-append
           #:object-size
           #:object-write
           #:object-missing
           #:object-missing-store
           #:object-missing-path
           #:object-out-of-date
           #:object-out-of-date-store
           #:object-out-of-date-path
           #:object-out-of-date-universal-time
           #:open-object-stream
           #:probe-object
           #:with-open-object-stream))

(in-package #:clpi/object-store-defs)

(define-condition object-missing ()
  ((store
    :initarg :store
    :reader object-missing-store)
   (path
    :initarg :path
    :reader object-missing-path))
  (:documentation
   "Condition stating that an object is missing from an object store."))

(define-condition object-out-of-date ()
  ((store
    :initarg :store
    :reader object-out-of-date-store)
   (path
    :initarg :path
    :reader object-out-of-date-path)
   (universal-time
    :initarg :universal-time
    :reader object-out-of-date-universal-time))
  (:documentation
   "Condition stating that an object is out of date."))

(defgeneric object-append (object-store path in-stream)
  (:documentation
   "Append the contents of IN-STREAM to the object in OBJECT-STORE located at PATH."))

(defgeneric object-size (object-store path)
  (:documentation
   "Returns the size of the object located at PATH in the OBJECT-STORE in bytes."))

(defgeneric object-write (object-store path in-stream)
  (:documentation
   "Write the contents of IN-STREAM to the object located at PATH in the
OBJECT-STORE. Overwrites the object if it exists."))

(defgeneric probe-object (object-store path)
  (:documentation
   "Returns non-NIL if the PATH exists in OBJECT-STORE."))

(defgeneric open-object-stream (object-store path direction
                                &key binary-p offset
                                  if-does-not-exist if-exists)
  (:documentation
   "Open a stream with the contents of the object from OBJECT-STORE located at PATH.

If BINARY-P is true, the returned stream is of type (UNSIGNED-BYTE 8).

If OFFSET is specified, the stream will start at the provided offset.

IF-DOES-NOT-EXIST should be NIL (in which case NIL is returned if the object
does not exist) or :ERROR to signal an error."))

(defgeneric call-with-open-object-stream (object-store path thunk
                                          &rest args
                                          &key direction
                                            binary-p offset
                                            if-does-not-exist
                                            if-exists))

(defmethod call-with-open-object-stream (object-store path thunk
                                         &rest args
                                         &key (direction :input)
                                           binary-p offset
                                           if-does-not-exist
                                           if-exists)
  (declare (ignore binary-p offset if-does-not-exist if-exists))
  (let ((stream (apply #'open-object-stream
                       object-store path direction
                       (remove-from-plist args :direction))))
    (if stream
        (with-open-stream (stream stream)
          (funcall thunk stream))
        (funcall thunk stream))))

(defmacro with-open-object-stream ((s object-store path
                                    &rest args
                                    &key
                                      (direction :input)
                                      binary-p
                                      offset
                                      if-does-not-exist
                                      if-exists)
                                   &body body)
  (declare (ignore binary-p offset if-does-not-exist if-exists direction))
  `(call-with-open-object-stream ,object-store ,path (lambda (,s) ,@body)
                                 ,@args))
