;;;; CLPI - Common Lisp Project Index
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/clpi
    (:nicknames #:clpi)
  (:use #:cl
        #:clpi/defs
        #:clpi/index
        #:clpi/object-store
        #:clpi/object-store-dual
        #:clpi/object-store-file
        #:clpi/object-store-http
        #:clpi/project
        #:clpi/release
        #:clpi/repos
        #:clpi/system
        #:clpi/system-file
        #:clpi/system-release
        #:clpi/utils)
  (:reexport #:clpi/defs
             #:clpi/index
             #:clpi/object-store
             #:clpi/object-store-dual
             #:clpi/object-store-file
             #:clpi/object-store-http
             #:clpi/project
             #:clpi/release
             #:clpi/repos
             #:clpi/system
             #:clpi/system-file
             #:clpi/system-release)
  (:export #:with-clpi-io-syntax))

(in-package #:clpi/clpi)
