;;;; Defs - Consolidates all defs into one package
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/defs
    (:use #:cl
          #:clpi/index-defs
          #:clpi/object-store-defs
          #:clpi/project-defs
          #:clpi/release-defs
          #:clpi/system-defs
          #:clpi/system-file-defs
          #:clpi/system-release-defs)
  (:reexport #:clpi/index-defs
             #:clpi/object-store-defs
             #:clpi/project-defs
             #:clpi/release-defs
             #:clpi/system-defs
             #:clpi/system-file-defs
             #:clpi/system-release-defs))

(in-package #:clpi/defs)
