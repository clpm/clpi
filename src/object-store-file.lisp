;;;; File based object store
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/object-store-file
    (:use #:cl
          #:alexandria
          #:clpi/defs
          #:clpi/object-store)
  (:export #:file-object-store))

(in-package #:clpi/object-store-file)

(defclass file-object-store (object-store)
  ((root
    :initarg :root
    :reader file-object-store-root
    :documentation
    "A directory pathname to the root of the object store."))
  (:documentation
   "Object storage on a file system."))

(defmethod initialize-instance :before ((object-store file-object-store) &key root &allow-other-keys)
  (unless (uiop:directory-pathname-p root)
    (error ":ROOT must be a directory pathname")))

(defmethod object-append ((object-store file-object-store) path in-stream)
  (let ((pn (merge-pathnames path (file-object-store-root object-store))))
    (ensure-directories-exist pn)
    (with-open-file (s pn
                       :element-type (stream-element-type in-stream)
                       :direction :output
                       :if-exists :append
                       :if-does-not-exist :create)
      (uiop:copy-stream-to-stream in-stream s
                                  :element-type (stream-element-type in-stream)))))

(defmethod object-size ((object-store file-object-store) path)
  (let ((pn (merge-pathnames path (file-object-store-root object-store))))
    (with-open-file (s pn
                       :element-type '(unsigned-byte 8)
                       :if-does-not-exist nil)
      (when s
        (file-length s)))))

(defun call-with-staging-pathname (thunk pn append-p)
  (uiop:with-staging-pathname (staging-pn pn)
    (when (and append-p (probe-file pn))
      (uiop:copy-file pn staging-pn))
    (funcall thunk staging-pn)))

(defmacro with-staging-pathname ((pn-var &key (pathname-value pn-var)
                                           append-p)
                                 &body body)
  `(call-with-staging-pathname (lambda (,pn-var) ,@body) ,pathname-value ,append-p))

(defmethod object-write ((object-store file-object-store) path in-stream)
  (let ((pn (merge-pathnames path (file-object-store-root object-store))))
    (ensure-directories-exist pn)
    (with-open-file (s pn
                       :element-type (stream-element-type in-stream)
                       :direction :output
                       :if-exists :supersede)
      (uiop:copy-stream-to-stream in-stream s :element-type (stream-element-type in-stream)))))

(defmethod probe-object ((object-store file-object-store) path)
  (let ((pn (merge-pathnames path (file-object-store-root object-store))))
    (probe-file pn)))

(defmethod call-with-open-object-stream ((object-store file-object-store) path thunk
                                         &rest args
                                         &key (direction :input)
                                           (if-exists :error)
                                         &allow-other-keys)
  (if (eql direction :input)
      (call-next-method)
      (let ((pn (merge-pathnames path (file-object-store-root object-store))))
        (ensure-directories-exist pn)
        (with-staging-pathname (pn :append-p (eql if-exists :append))
          (apply #'call-next-method
                 object-store
                 (enough-namestring pn (file-object-store-root object-store))
                 thunk args)))))

(defmethod open-object-stream ((object-store file-object-store) path (direction (eql :output))
                               &rest open-args
                               &key binary-p offset
                                 if-does-not-exist (if-exists :error))
  (assert (null offset))
  (let* ((pn (merge-pathnames path (file-object-store-root object-store)))
         (open-args (remove-from-plist open-args :binary-p :offset :if-exists)))
    (when binary-p
      (push '(unsinged-byte 8) open-args)
      (push :element-type open-args))
    (apply #'open pn :direction :output
                     :if-exists if-exists
                     open-args)))

(defmethod open-object-stream ((object-store file-object-store) path (direction (eql :input))
                               &key binary-p offset
                                 (if-does-not-exist :error) if-exists)
  (declare (ignore if-exists))
  (let* ((pn (merge-pathnames path (file-object-store-root object-store)))
         (open-args nil)
         (exists-p (probe-file pn)))
    (when binary-p
      (push '(unsinged-byte 8) open-args)
      (push :element-type open-args))
    (unless exists-p
      (ecase if-does-not-exist
        ((nil)
         (signal 'object-missing :store object-store :path path)
         (return-from open-object-stream nil))
        (:error
         (error 'object-missing :store object-store :path path))))
    (let ((s (apply #'open pn open-args))
          (normal-exit-p))
      (unwind-protect
           (progn
             (when offset
               (file-position s offset))
             (setf normal-exit-p t)
             s)
        (unless normal-exit-p
          (close s))))))
