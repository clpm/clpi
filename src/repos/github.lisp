;;;; Github repositories
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/repos/github
    (:use #:cl
          #:clpi/repos/defs)
  (:export #:github-repo
           #:github-repo-host
           #:github-repo-path))

(in-package #:clpi/repos/github)

(defclass github-repo ()
  ((host
    :initarg :host
    :initform "github.com"
    :reader github-repo-host)
   (path
    :initarg :path
    :initform (error "Path must be provided")
    :reader github-repo-path)))

(defmethod repo-to-description ((repo github-repo))
  `(:github
    ,@(when (not (equal (github-repo-host repo) "github.com"))
        (list :host (github-repo-host repo)))
    :path ,(github-repo-path repo)))
