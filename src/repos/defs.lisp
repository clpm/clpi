;;;; Repository definitions
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/repos/defs
    (:use #:cl)
  (:export #:repo-to-description))

(in-package #:clpi/repos/defs)

(defgeneric repo-to-description (repo)
  (:documentation
   "Return a description of REPO suitable for reconstructing it with
MAKE-REPO-FROM-DESCRIPTION."))
