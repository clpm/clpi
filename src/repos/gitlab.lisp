;;;; Gitlab repositories
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi/repos/gitlab
    (:use #:cl
          #:clpi/repos/defs)
  (:export #:gitlab-repo
           #:gitlab-repo-host
           #:gitlab-repo-path))

(in-package #:clpi/repos/gitlab)

(defclass gitlab-repo ()
  ((host
    :initarg :host
    :initform "gitlab.com"
    :reader gitlab-repo-host)
   (path
    :initarg :path
    :initform (error "Path must be provided")
    :reader gitlab-repo-path)))

(defmethod repo-to-description ((repo gitlab-repo))
  `(:gitlab
    ,@(when (not (equal (gitlab-repo-host repo) "gitlab.com"))
        (list :host (gitlab-repo-host repo)))
    :path ,(gitlab-repo-path repo)))
