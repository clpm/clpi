;;;; Very simple smoke tests
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi-test/simple
    (:use #:cl
          #:alexandria
          #:clpi
          #:clpi-test/framework
          #:clpi-test/utils
          #:clpi-test/web-server
          #:fiveam)
  (:import-from #:clpm-multi-http-client
                #:*default-http-client*)
  (:import-from #:clpm-multi-http-client-impl/drakma
                #:drakma-client))

(in-package #:clpi-test/simple)

(def-suite :clpi/simple
  :in :clpi)

(defun run-simple-index-tests (index)
  ;; There is no project named b:
  (signals index-project-missing (index-project index "b"))
  (is (null (index-project index "b" nil)))
  ;; The only project is named a:
  (let ((project (index-project index "a")))
    (is-true project)
    ;; It has no version 0.0.0
    (signals project-no-such-version (project-release project "0.0.0"))
    (is (equal (list nil nil) (multiple-value-list (project-release project "0.0.0" nil))))

    ;; Version has a release 0.1.0
    (multiple-value-bind (release exists-p)
        (project-release project "0.2.0")
      (is-true release)
      (is-true exists-p)
      (is (equal "https://localhost/a-v0.2.0.tar.gz" (release-url release)))
      ;; There is no b.asd
      (signals release-no-such-system-file (release-system-file release "b.asd"))
      (is (null (release-system-file release "b.asd" nil)))
      ;; Version 0.1.0 has a system-file for a.asd
      (let ((system-file (release-system-file release "a.asd" nil)))
        (is-true system-file)
        (let ((system-release (system-file-system-release system-file "a")))
          (is-true system-release)
          (is (equal "0.2.0" (system-release-version system-release)))
          (is (set-equal '("alexandria") (system-release-dependencies system-release)
                         :test #'equal)))))))

(def-test simple-file-test (:suite :clpi/simple)
  (let ((index (make-instance 'index
                              :object-store (make-instance 'file-object-store
                                                           :root (test-index-root-pathname "v0.4/simple/")))))
    (run-simple-index-tests index)))

(def-test simple-http-test (:suite :clpi/simple)
  (let ((*default-http-client* (make-instance 'drakma-client)))
    (with-clpi-index-server (url "v0.4/simple/")
      (let ((index (make-instance 'index
                                  :object-store
                                  (make-instance 'http-object-store :url url))))
        (run-simple-index-tests index)))))

(def-test simple-dual-test (:suite :clpi/simple)
  (let ((*default-http-client* (make-instance 'drakma-client)))
    (with-clpi-index-server (url "v0.4/simple/")
      (with-temporary-directory (file-root)
        (let ((index (make-instance 'index
                                    :object-store
                                    (make-instance 'dual-object-store
                                                   :primary (make-instance 'http-object-store
                                                                           :url url)
                                                   :secondary (make-instance 'file-object-store
                                                                             :root file-root)))))
          (run-simple-index-tests index))))))
