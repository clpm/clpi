;;;; Very simple modification tests
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi-test/modification
    (:use #:cl
          #:fiveam
          #:clpi
          #:clpi-test/framework
          #:clpi-test/utils
          #:clpi-test/web-server))

(in-package #:clpi-test/modification)

(def-suite :clpi/modification
  :in :clpi)

;; (defun run-simple-modification-tests (index)
;;   (let* ((project (index-project index "a"))
;;          (system (index-system index "a"))
;;          (new-release (make-instance 'release
;;                                      :project project
;;                                      :version "0.3.0"
;;                                      :url "https://localhost/a-v0.3.0.tar.gz"
;;                                      :systems (list "a")
;;                                      :system-files (list "a.asd")))
;;          (new-system-release (make-instance 'system-release
;;                                             :system-file "a.asd"
;;                                             :dependencies (list "alexandria")
;;                                             :version "0.3.0"
;;                                             :system system
;;                                             :project "a"
;;                                             :project-version "0.3.0")))
;;     (project-add-release! project new-release)
;;     (system-add-system-release! system new-system-release)))

;; (def-test simple-modification-test (:suite :clpi/modification)
;;   (with-temporary-directory (file-root :keep t)
;;     (copy-directory (test-index-root-pathname "v0.4/simple/") file-root)
;;     (let ((index (make-instance 'file-index :root file-root)))
;;       (run-simple-modification-tests index)
;;       (index-save! index))
;;     ;; (let ((x (make-instance 'file-index :root file-root))
;;     ;;       (y (make-instance 'file-index :root (test-index-root-pathname "v0.4/mod-results/"))))
;;     ;;   (is (clpi-equal-p x y)))
;;     ))
