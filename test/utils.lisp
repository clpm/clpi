;;;; Utils
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi-test/utils
    (:use #:alexandria
          #:cl)
  (:export #:copy-directory
           #:directory-contents-equal
           #:test-index-root-pathname
           #:with-temporary-directory))

(in-package #:clpi-test/utils)

(defun test-index-root-pathname (path)
  (asdf:system-relative-pathname :clpi-test (uiop:strcat "test/indices/" path)))

(defun call-with-temporary-directory (thunk &key keep)
  (let* ((tmp-file-pathname (uiop:tmpize-pathname (merge-pathnames "clpi" (uiop:temporary-directory))))
         (tmp-dir-pathname (uiop:ensure-directory-pathname tmp-file-pathname)))
    (uiop:delete-file-if-exists tmp-file-pathname)
    (ensure-directories-exist tmp-dir-pathname)
    (unwind-protect
         (funcall thunk tmp-dir-pathname)
      (unless keep
        (uiop:delete-directory-tree tmp-dir-pathname :validate t)))))

(defmacro with-temporary-directory ((pathname &key keep) &body body)
  `(call-with-temporary-directory (lambda (,pathname) ,@body) :keep ,keep))

(defun file-contents-equal (dir-1 dir-2 namestring)
  (with-open-file (stream-1 (merge-pathnames namestring dir-1)
                            :element-type '(unsigned-byte 8))
    (with-open-file (stream-2 (merge-pathnames namestring dir-2)
                              :element-type '(unsigned-byte 8))
      (and (= (file-length stream-1) (file-length stream-2))
           (loop
             :for byte-1 := (read-byte stream-1 nil)
             :for byte-2 := (read-byte stream-2 nil)
             :while byte-1
             :always (= byte-1 byte-2))))))

(defun directory-contents-equal (dir-1 dir-2)
  (setf dir-1 (uiop:ensure-directory-pathname dir-1))
  (setf dir-2 (uiop:ensure-directory-pathname dir-2))
  (and (uiop:directory-exists-p dir-1)
       (uiop:directory-exists-p dir-2)
       (let ((dir-1-files (uiop:directory-files dir-1))
             (dir-2-files (uiop:directory-files dir-2)))
         (and (set-equal dir-1-files dir-2-files
                         :test #'equal :key #'file-namestring)
              (every (curry #'file-contents-equal dir-1 dir-2)
                     (mapcar #'file-namestring dir-1-files))))
       (let ((dir-1-subdirs (uiop:subdirectories dir-1))
             (dir-2-subdirs (uiop:subdirectories dir-2)))
         (and (set-equal dir-1-subdirs dir-2-subdirs
                         :test #'equal :key (compose #'last-elt #'pathname-directory))
              (every (lambda (sub)
                       (directory-contents-equal (merge-pathnames sub dir-1)
                                                 (merge-pathnames sub dir-2)))
                     (mapcar (compose #'last-elt #'pathname-directory)
                             dir-1-subdirs))))))

(defun copy-directory-files (src dest)
  (setf src (uiop:ensure-directory-pathname src))
  (setf dest (uiop:ensure-directory-pathname dest))
  (ensure-directories-exist dest)
  (loop
    :for file :in (uiop:directory-files src)
    :for namestring := (file-namestring file)
    :do (uiop:copy-file file (merge-pathnames namestring dest))))

(defun copy-directory (src dest)
  (setf src (uiop:ensure-directory-pathname src))
  (setf dest (uiop:ensure-directory-pathname dest))
  (uiop:collect-sub*directories src t t
                                (lambda (new-src)
                                  (copy-directory-files new-src
                                                        (merge-pathnames
                                                         (enough-namestring new-src src)
                                                         dest)))))
