;;;; Test entrypoint
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi-test/clpi-test
    (:nicknames #:clpi-test)
  (:use #:cl
        #:clpi-test/framework
        #:clpi-test/modification
        #:clpi-test/simple))

(in-package #:clpi-test/clpi-test)
