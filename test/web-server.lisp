;;;; A simple webserver for testing HTTP indices.
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi-test/web-server
    (:use #:cl
          #:alexandria
          #:clpi-test/utils
          #:hunchentoot)
  (:export #:with-clpi-index-server))

(in-package #:clpi-test/web-server)

(defclass clpi-acceptor (acceptor)
  ())

(defmethod acceptor-dispatch-request ((acceptor clpi-acceptor) request)
  "Detault implementation of the request dispatch method, generates an
+http-not-found+ error."
  (let ((path (and (acceptor-document-root acceptor)
                   (starts-with-subseq "clpi/v0.4/" (namestring (request-pathname request)))
                   (subseq (namestring (request-pathname request)) 10))))
    (cond
      (path
       (handle-static-file
        (merge-pathnames path (acceptor-document-root acceptor))
        "text/plain; charset=utf-8"))
      (t
       (setf (return-code *reply*) +http-not-found+)
       (abort-request-handler)))))

(defun call-with-clpi-index-server (path thunk)
  (let* ((document-root (test-index-root-pathname path))
         (acceptor (make-instance 'clpi-acceptor
                                  :document-root document-root
                                  :access-log-destination nil
                                  :port 0
                                  :address "127.0.0.1")))
    (start acceptor)
    (unwind-protect
         (funcall thunk (format nil "http://localhost:~D/" (acceptor-port acceptor)))
      (stop acceptor))))

(defmacro with-clpi-index-server ((url path) &body body)
  `(call-with-clpi-index-server ,path (lambda (,url) ,@body)))
