;;;; Define the test framework
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpi-test/framework
    (:use #:cl
          #:fiveam))

(in-package #:clpi-test/framework)

(def-suite :clpi
  :description "Tests for the CLPI library.")
