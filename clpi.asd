;;;; CLPI System Definition
;;;;
;;;; This software is part of CLPI. See README.org for more information. See
;;;; LICENSE for license information.

#-:asdf3.2
(error "Requires ASDF >=3.2")

;; Not necessary, but easier to have when using SLIME.
(in-package :asdf-user)

(defsystem #:clpi
  :version "0.0.1"
  :description "A library to interact with and create Common Lisp Project Indices."
  :license "BSD-2-Clause"
  :pathname "src/"
  :class :package-inferred-system
  :depends-on (#:clpi/clpi)
  :in-order-to ((test-op (load-op "clpi-test")))
  :perform (test-op (op c) (unless (symbol-call :fiveam :run! :clpi)
                             (error "Test failure"))))
